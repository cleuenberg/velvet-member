<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

?>

<?php if ( is_front_page() ) : $post_class = 'ast-col-sm-12'; else : $post_class = ''; endif; ?>

<?php astra_entry_before(); ?>

<?php if ( is_front_page() ) : ?>
	<?php if (wp_get_attachment_url( get_post_thumbnail_id() )) : ?>
		<style>
			article#post-<?php the_ID(); ?> {
				position: relative;
				z-index:1;
			}
			article#post-<?php the_ID(); ?>:after {
				content : "";
				display: block;
				position: absolute;
				top: 0;
				left: 0;
				background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>); 
				background-position: center;
				width: 100%;
				height: 100%;
				opacity : 0.2;
				z-index: -1;
			}
		</style>
	<?php endif; ?>
<?php endif; ?>

<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>

	<?php astra_entry_top(); ?>

	<header class="entry-header <?php astra_entry_header_class(); ?>">

		<?php astra_the_title( sprintf( '<h2 class="entry-title" itemprop="headline"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-content clear" itemprop="text">

		<?php astra_entry_content_before(); ?>

		<?php
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. */
						__( 'Continue reading %s', 'astra' ) . ' <span class="meta-nav">&rarr;</span>', array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				)
			);
		?>

		<?php astra_entry_content_after(); ?>

		<?php
			wp_link_pages(
				array(
					'before'      => '<div class="page-links">' . esc_html( astra_default_strings( 'string-single-page-links-before', false ) ),
					'after'       => '</div>',
					'link_before' => '<span class="page-link">',
					'link_after'  => '</span>',
				)
			);
		?>
	</div><!-- .entry-content .clear -->

	<footer class="entry-footer">
		<?php astra_entry_footer(); ?>
	</footer><!-- .entry-footer -->

	<?php astra_entry_bottom(); ?>

</article><!-- #post-## -->

<?php astra_entry_after(); ?>
