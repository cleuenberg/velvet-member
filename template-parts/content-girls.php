<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

?>

<?php astra_entry_before(); ?>

<?php if (pms_is_post_restricted(get_the_ID())) : ?>

<?php else : ?>

    <?php
        $post_class = get_post_class();
        if ( ($key = array_search('ast-col-sm-12', $post_class) ) !== false) {
            unset($post_class[$key]);
        }
        $post_class = 'class="' . implode(" ", $post_class) . '"';
    ?>

    <article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-<?php the_ID(); ?>" <?php echo $post_class; ?>>

    	<?php astra_entry_top(); ?>

    	<?php astra_entry_content_blog(); ?>

    	<?php astra_entry_bottom(); ?>

    </article><!-- #post-## -->

<?php endif; ?>

<?php astra_entry_after(); ?>
