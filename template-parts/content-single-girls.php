<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

?>

<?php astra_entry_before(); ?>

<section class="velvet-headline">
    <h1><?php the_title(); ?></h1>
</section>

<article itemtype="https://schema.org/CreativeWork" itemscope="itemscope" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php astra_entry_top(); ?>

    <?php the_content(); ?>

	<?php astra_entry_bottom(); ?>

</article><!-- #post-## -->


<?php
$images = get_field('galerie');
if ($images) :
?>
<article id="gallery-<?php the_ID(); ?>" <?php post_class('gallery'); ?>>
    <h3><?php _e('Bildergalerie', 'velvet'); ?></h3>

    <ul class="gallery-thumbnails">
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo wp_get_attachment_image_url( $image['ID'], 'full', false ); ?>"><?php echo wp_get_attachment_image( $image['ID'], 'thumbnail' ); ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</article>
<?php endif; ?>

<?php astra_entry_after(); ?>
