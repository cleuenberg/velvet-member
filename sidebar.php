<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

$sidebar = apply_filters( 'astra_get_sidebar', 'sidebar-1' );

?>

<div itemtype="https://schema.org/WPSideBar" itemscope="itemscope" id="secondary" <?php astra_secondary_class(); ?> role="complementary">

	<div class="sidebar-main">

		<?php astra_sidebars_before(); ?>

        <?php if ( is_singular('girls') ) : ?>
            <aside class="widget widget_image">
                <p><?php the_post_thumbnail( 'image_300' ); ?></p>
            </aside>
            <?php /*
            <aside class="widget widget_cta">
                <?php $membercard = do_shortcode('[user_meta key="custom_card_number" wpautop="off"]'); ?>
                <p><a href="/reservierung/?girl=<?php the_title();?>&amp;membercard=<?php echo $membercard; ?>" class="ast-button velvet-button"><i class="fa fa-envelope"></i> <?php the_title();?> <?php _e('reservieren','velvet'); ?></a></p>
            </aside>
            */ ?>
        <?php endif; ?>

		<?php if ( is_active_sidebar( $sidebar ) ) : ?>

			<?php dynamic_sidebar( $sidebar ); ?>

		<?php endif; ?>

		<?php astra_sidebars_after(); ?>

	</div><!-- .sidebar-main -->
</div><!-- #secondary -->
