/* Custom JS Functions */
jQuery(document).ready(function($) {
    
    /* Profile Builder / Paid Member Subscriptions */

    // translate en into de
    $('#pms-paygates-inner input[value="paypal_express"] + .pms-paygate-name').text('PayPal / Kreditkarte');
    $('#pms-paygates-inner input[value="paypal_pro"] + .pms-paygate-name').text('Kreditkarte');
    $('ul.pms-credit-card-information > li.pms-field-type-heading > h4').text('Kreditkarten-Informationen');
    $('ul.pms-credit-card-information label[for="pms_card_number"]').text('Kreditkartennummer');
    $('ul.pms-credit-card-information label[for="pms_card_cvv"]').text('Prüfnummer CVV');
    $('ul.pms-billing-details > li.pms-field-type-heading > h4').text('Rechnungsempfänger');
    $('ul.pms-billing-details label[for="pms_billing_first_name"]').text('Vorname');
    $('ul.pms-billing-details label[for="pms_billing_last_name"]').text('Nachname');
    $('ul.pms-billing-details label[for="pms_billing_address"]').text('Adresse');
    $('ul.pms-billing-details label[for="pms_billing_city"]').text('Ort');
    $('ul.pms-billing-details label[for="pms_billing_zip"]').text('Postleitzahl');
    $('ul.pms-billing-details label[for="pms_billing_country"]').text('Land');
    $('ul.pms-billing-details label[for="pms_billing_state"]').text('Kanton/Bundesland/Staat');
    $('#pms-subscription-plans-discount label[for="pms_subscription_plans_discount"]').text('Gutscheincode');
    $('#pms_subscription_plans_discount_code').attr('placeholder', 'Gutscheincode eingeben');
    $('#pms-apply-discount').val('einlösen');

    // preselects
    $('#pms_billing_country option[value="CH"]').attr('selected', 'selected');
    $('#send_credentials_via_email').attr('checked', 'checked');

    // select payment method depending on delivery
    $('.wppb-form-field.wppb-radio input[name="custom_field_lieferung"]').change(function() {
        var delivery = $(this).val();
        if (delivery == 'lieferung') {
            var $radios_payment = $('#pms-paygates-inner input[name="pay_gate"]');
            $radios_payment.filter('[value^=paypal_]').prop('checked', true);
            $radios_payment.filter('[value=manual]').attr('disabled', true);
            $('#custom_field_payment').val('PayPal / Kreditkarte');
        } else {
            var $radios_payment = $('#pms-paygates-inner input[name="pay_gate"]');
            $radios_payment.filter('[value=manual]').attr('disabled', false);
        }
    });

    // populate hidden fields: payment method / ip address
    //$('#custom_field_payment').val('');
    $('#pms-paygates-inner input[value="manual"] + span.pms-paygate-name').text('Barzahlung im Laden');
    $('#pms-paygates-inner input[name="pay_gate"]').change(function() {
        var payment = $(this).val();
        var payment_label = '';
        if (payment == 'manual') {
            payment_label = 'Barzahlung im Laden';
        } else if (payment == 'paypal_express') {
            payment_label = 'PayPal / Kreditkarte';
        } else if (payment == 'paypal_standard') {
            payment_label = 'PayPal';
        }
        $('#custom_field_payment').val(payment_label);
    });
    $('#custom_field_ipaddress').val( $('#user_ipaddress_on_register').text() );


    /* Filter Table */

    // init
    $('table.user-entries').filterTable({minRows: 0});

    // translate
    $('p.filter-table input').attr('placeholder', 'Suchwort eingeben');


    /* Home */
    $('.home.page-template-page-start article.type-post p').text(function() {
        if ($(this).text() == ' …') {
            $(this).hide();
        }
    });
});