jQuery(document).ready(function ($) {

    if ( $('.quform').length ) {

        var $timePicker = $('.quform-field-1_9').data('kendoTimePicker');
     
        if ($timePicker && window.timePickerTimes) {
            var dates = [], parts;
     
            for (var i = 0; i < timePickerTimes.length; i++) {
                parts = timePickerTimes[i].split(':');
                dates.push(new Date(2000, 0, 1, parseInt(parts[0], 10), parseInt(parts[1], 10)));
            }
     
            $timePicker.setOptions({
                dates: dates
            });
        }

        function set_min_time() {
            var chosen_date = $('.quform-field-1_8').val();
            
            var today = new Date();
            var dd    = today.getDate();
            var mm    = today.getMonth()+1; 
            var yyyy  = today.getFullYear();
            var hrs   = today.getHours()+1;
            var mns   = today.getMinutes();
            if(dd<10) { dd='0'+dd; } 
            if(mm<10) { mm='0'+mm; } 
            var current_date = dd+'.'+mm+'.'+yyyy;

            if (chosen_date == current_date) {
                //timePickerTimes = ["14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45","16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45","19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45","22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45","00:00"];
                $timePicker.setOptions({
                    dates: new Date(2000, 0, 1, parseInt(hrs+1), 0)
                });

                mns = parseInt(mns);

                if (mns > 0 && mns <= 15) {
                    mns = 15;
                } else if (mns > 15 && mns <= 30) {
                    mns = 30;
                } else if (mns > 30 && mns <= 45) {
                    mns = 45;
                } else if (mns > 45 && mns < 60) {
                    mns = 0;
                }

                $timePicker.setOptions({
                    min: new Date(2000, 0, 1, parseInt(hrs), parseInt(mns)),
                    max: new Date(2000, 0, 1, 0, 0, 0)
                });
                $('.quform-field-1_9.quform-field-time').val('');
            } else {
                $timePicker.setOptions({
                    min: new Date(2000, 0, 1, 0, 15, 0)
                });

                if ($timePicker && window.timePickerTimes) {
                    var dates = [], parts;
             
                    for (var i = 0; i < timePickerTimes.length; i++) {
                        parts = timePickerTimes[i].split(':');
                        dates.push(new Date(2000, 0, 1, parseInt(parts[0], 10), parseInt(parts[1], 10)));
                    }
             
                    $timePicker.setOptions({
                        dates: dates
                    });
                }
            }
        }

        set_min_time();
        $('.quform-field-date.quform-field-1_8').change(set_min_time);

        //https://docs.telerik.com/kendo-ui/api/javascript/ui/timepicker

    }
});