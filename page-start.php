<?php
/**
 * Template Name: Startseite
 *
 */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) :
				the_post();
?>

				<?php if ( is_user_logged_in() ) : ?>

					<?php
					// check if the flexible content field has rows of data
					if ( have_rows('teaser-block') ) :

						$count_teaser 				= 1;
						$teaser_background 			= '';
						$teaser_background_color 	= '';
						$teaser_inner_css 			= 'u-text-left';
						$text_color					= '';

					     // loop through the rows of data
					    while ( have_rows('teaser-block') ) : the_row();

					        if ( get_row_layout() == 'teaser-block' ) :

					        	if ($count_teaser % 2 == 0) :
					        		$teaser_inner_css = 'u-text-right';
					        	else :
					        		$teaser_inner_css = 'u-text-left';
					        	endif;

					        	if ( get_sub_field('wallpaper') ) :
					        		$teaser_background = get_sub_field('wallpaper');
					        	else :
					        		$teaser_background = '';
					        	endif;

					        	if ( get_sub_field('font-color') ) :
					        		$text_color = get_sub_field('font-color');
					        	else :
					        		$text_color = '';
					        	endif;
					?>

					        	<article class="ast-article-single home-teaser <?php the_sub_field('css-classes'); ?>" id="teaser-block-<?php echo $count_teaser; ?>" style="background-image:url(<?php echo wp_get_attachment_url($teaser_background); ?>);">
					        		<div class="entry-content clear <?php echo $teaser_inner_css; ?>">

					        			<?php if (get_sub_field('headline')) : ?>
					        				<h3 style="color:<?php echo $text_color; ?>;"><?php the_sub_field('headline'); ?></h3>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('teaser-text')) : ?>
					        				<div style="color:<?php echo $text_color; ?>;"><?php the_sub_field('teaser-text'); ?></div>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('link')) : ?>
					        				<a href="<?php the_sub_field('link'); ?>" class="ast-button velvet-button"><?php the_sub_field('button-text'); ?></a>
					        			<?php endif; ?>

					        		</div>
					        	</article>

					<?php
							elseif ( get_row_layout() == 'cta-block' ) :
					?>

								<article class="ast-article-single home-teaser cta-block" id="teaser-block-<?php echo $count_teaser; ?>" style="background-color:<?php the_sub_field('background-color'); ?>;">
					        		<div class="entry-content clear">

					        			<?php if (get_sub_field('text')) : ?>
					        				<h4><?php the_sub_field('text'); ?></h4>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('link')) : ?>
					        				<a href="<?php the_sub_field('link'); ?>" class="ast-button velvet-button"><?php the_sub_field('button-text'); ?></a>
					        			<?php endif; ?>

					        		</div>
					        	</article>

					<?php
					        endif;
					    	$count_teaser++;
					    endwhile;
					endif;
					?>

					<?php 
					$latest_blog_posts = new WP_Query( array( 'posts_per_page' => 3, 'category_name' => 'startseite' ) );

					if ( $latest_blog_posts->have_posts() ) : 
						//$post_count = $latest_blog_posts->post_count;
					?>
						<div class="ast-row" style="margin-top:4em;">

							<?php
							while ( $latest_blog_posts->have_posts() ) : 
								$latest_blog_posts->the_post();
	    						get_template_part( 'template-parts/content', astra_get_post_format() );
							endwhile;
							?>

						</div>
					<?php
					endif;
					?>

				<?php else : ?>

					<?php if (!empty(get_the_content())) : ?>
						<?php get_template_part( 'template-parts/content', 'page' ); ?>
					<?php endif; ?>

					<?php
					// check if the flexible content field has rows of data
					if ( have_rows('teaser-block-guest') ) :

						$count_teaser 				= 1;
						$teaser_background 			= '';
						$teaser_background_color 	= '';
						$teaser_inner_css 			= 'u-text-left';
						$text_color					= '';

					     // loop through the rows of data
					    while ( have_rows('teaser-block-guest') ) : the_row();

					    	if ( get_row_layout() == 'teaser-block-banner' ) :
					?>
								<article class="ast-article-single home-teaser home-teaser-banner" id="teaser-block-<?php echo $count_teaser; ?>">
					        		<div class="entry-content clear">
					        			<?php if (get_sub_field('link')) : ?><a href="<?php the_sub_field('link'); ?>"><?php endif; ?>
					        				<?php echo wp_get_attachment_image(get_sub_field('bild'), 'full'); ?>
					        			<?php if (get_sub_field('link')) : ?></a><?php endif; ?>
					        		</div>
					        	</article>
					<?php
					        elseif ( get_row_layout() == 'teaser-block' ) :

					        	if ($count_teaser % 2 == 0) :
					        		$teaser_inner_css = 'u-text-right';
					        	else :
					        		$teaser_inner_css = 'u-text-left';
					        	endif;

					        	if ( get_sub_field('wallpaper') ) :
					        		$teaser_background = get_sub_field('wallpaper');
					        	else :
					        		$teaser_background = '';
					        	endif;

					        	if ( get_sub_field('font-color') ) :
					        		$text_color = get_sub_field('font-color');
					        	else :
					        		$text_color = '';
					        	endif;
					?>

								<article class="ast-article-single home-teaser" id="teaser-block-<?php echo $count_teaser; ?>" style="background-image:url(<?php echo wp_get_attachment_url($teaser_background); ?>);">
					        		<div class="entry-content clear <?php echo $teaser_inner_css; ?>">

					        			<?php if (get_sub_field('headline')) : ?>
					        				<h3 style="color:<?php echo $text_color; ?>;"><?php the_sub_field('headline'); ?></h3>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('teaser-text')) : ?>
					        				<div style="color:<?php echo $text_color; ?>;"><?php the_sub_field('teaser-text'); ?></div>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('link')) : ?>
					        				<a href="<?php the_sub_field('link'); ?>" class="ast-button velvet-button"><?php the_sub_field('button-text'); ?></a>
					        			<?php endif; ?>

					        		</div>
					        	</article>

					<?php
							elseif ( get_row_layout() == 'cta-block' ) :
					?>

								<article class="ast-article-single home-teaser cta-block" id="teaser-block-<?php echo $count_teaser; ?>" style="background-color:<?php the_sub_field('background-color'); ?>;">
					        		<div class="entry-content clear">

					        			<?php if (get_sub_field('text')) : ?>
					        				<h4 style="color:<?php the_sub_field('font-color'); ?>;"><?php the_sub_field('text'); ?></h4>
					        			<?php endif; ?>
					        			<?php if (get_sub_field('link')) : ?>
					        				<a href="<?php the_sub_field('link'); ?>" class="ast-button velvet-button"><?php the_sub_field('button-text'); ?></a>
					        			<?php endif; ?>

					        		</div>
					        	</article>

					<?php
					        endif;
					    	$count_teaser++;
					    endwhile;
					endif;
					?>
				<?php endif; ?>

			<?php endwhile; ?>

		</main><!-- #main -->

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
