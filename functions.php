<?php
/**
 * Villa-Velvet Member Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Villa-Velvet Member
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'VILLA_VELVET_MEMBER_THEME_VERSION', '1.0.0' );
define( 'ASTRA_CHILD_THEME_DIR', get_stylesheet_directory() . '/' );

/**
 * Includes
 */
require_once ASTRA_CHILD_THEME_DIR . 'inc/acf-create-fields.php'; // ACF – Advanced Custom Fields

/**
 * ACF
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'Villa-Velvet Options'
    ));
}

/**
 * Enqueue styles
 */
function child_enqueue_styles() {
    wp_enqueue_style( 'fontawesome-style', get_stylesheet_directory_uri() . '/css/font-awesome.min.css' );
    wp_enqueue_style( 'villa-velvet-member-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), VILLA_VELVET_MEMBER_THEME_VERSION, 'all' );
    wp_enqueue_script( 'villa-velvet-member-theme-script', get_stylesheet_directory_uri() . '/js/functions.min.js', array( 'jquery' ), '20180330', true );
    wp_enqueue_script( 'villa-velvet-member-quform-script', get_stylesheet_directory_uri() . '/js/quform.custom.min.js', array( 'jquery' ), '20181015', true );
    wp_enqueue_script( 'filtertable-script', get_stylesheet_directory_uri() . '/js/jquery.filtertable.min.js', array( 'jquery' ), '1.5.7', true );
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/**
 * Add image sizes
 */
function velvet_custom_image_sizes() {
    add_image_size( 'image_300', 300, '', true );
}
add_action('after_setup_theme','velvet_custom_image_sizes');

/**
 * Change archive title
 */
add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_category() ) {
        $title = single_cat_title( '', false );
    } else if ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    }
    return $title;
});

/**
 * Adds custom classes to the array of post grid classes.
 */
if ( ! function_exists( 'velvet_post_class_blog_grid' ) ) {
    function velvet_post_class_blog_grid( $classes ) {

        if ( is_post_type_archive('girls') ) {
            //add class
            $classes[] = 'ast-col-sm-4';
        }

        return $classes;
    }
}
add_filter( 'post_class', 'velvet_post_class_blog_grid' );

/**
 * Custom button
 */
function velvet_button( $title='', $link='#', $icon='' ) {
    if ($icon != '') :
        $icon = '<i class="fa fa-' . $icon . '"></i> ';
    endif;

    echo '<a href="' . $link . '" class="ast-button velvet-button">' . $icon . $title . '</a>';
}

/**
 * Quform: dynamic value
 */
function velvet_quform_set_parameter_membercard( $value ) {
    $membercard = do_shortcode( '[user_meta key="custom_card_number" wpautop="off"]', false );
    return $membercard;
}
add_filter('quform_element_value_membercard', 'velvet_quform_set_parameter_membercard');

/**
 * Users: get current role
 */
function velvet_get_user_role( $user = null ) {
    $user = $user ? new WP_User( $user ) : wp_get_current_user();
    return $user->roles ? $user->roles[0] : false;
}

/**
 * Users: get subscription status
 */
function velvet_get_user_subscription( $user = null, $meta = 'status' ) {
    $user = $user ? new WP_User( $user ) : wp_get_current_user();
    $user_id = $user->ID;
    global $wpdb;
    $results = $wpdb->get_row( "SELECT `$meta` FROM {$wpdb->prefix}pms_member_subscriptions WHERE `user_id` = $user_id LIMIT 1" );
    return $results->$meta;
}

/*
 * List users shortcode.
 * Usage: [list_users]
 */
function wppbc_list_users( $atts, $content ) {

    if (current_user_can('edit_pages')) :

        global $wpdb;
        $users = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}pms_member_subscriptions" );
     
        if ( !empty( $users ) ) {
            $list = '<table class="user-entries">';
            $list .= '<thead>';
            $list .= '<tr>';
            $list .= '<th>Name</th>';
            $list .= '<th>Kartennummer</th>';
            $list .= '<th>Gültig bis</th>';
            $list .= '<th>Bezahlt</th>';
            $list .= '<th>&nbsp;</th>';
            $list .= '</tr>';
            $list .= '</thead>';
            $list .= '<tbody>';
            foreach ( $users as $user ) {
                $userdata = get_user_by('id', $user->user_id);
                if ( $userdata ) {
                    $list .= '<tr>';
                        $list .= '<td>' . get_the_author_meta('first_name', $userdata->ID) . ' ' . get_the_author_meta('last_name', $userdata->ID) . '</td>';
                        $list .= '<td>' . get_the_author_meta('custom_card_number', $userdata->ID) . '</td>';

                        $phpdate    = strtotime( velvet_get_user_subscription($userdata, 'expiration_date') );
                        $mysqldate  = date( 'd.m.Y', $phpdate );
                        $list .= '<td>' . $mysqldate . '</td>';

                        switch ( velvet_get_user_subscription($userdata) ) {
                            case 'active':
                                $list .= '<td><span style="color:green;">ja</span></td>';
                                break;
                            
                            case 'pending':
                                $list .= '<td><span style="color:red;">nein</span></td>';
                                break;

                            case 'expired':
                                $list .= '<td><span style="color:orange;">abgelaufen</span></td>';
                                break;
                            
                            default:
                                $list .= '<td><span style="color:orange;">'.velvet_get_user_subscription($userdata).'</span></td>';
                                break;
                        }

                        $list .= '<td class="u-text-center"><a href="' . get_page_link(224) . '?edit_user=' . $userdata->ID . '"><i class="fa fa-edit"></i></a></td>';
                    $list .= '</tr>';
                }
            }
            $list .= '</tbody>';
            $list .= '</table>';
        }
        return $list;

    else :
        return __('Keine Berechtigung …', 'velvet');
    endif;
}
add_shortcode( 'list_users', 'wppbc_list_users' );

/**
 * Register: change button text
 */
/*
function velvet_change_register_submit_text() {
    return __('Registrieren', 'velvet');
}
add_filter('pms_register_form_submit_text', 'velvet_change_register_submit_text');
*/

/**
 * Register: change error text
 */
function velvet_change_payment_error_message($error, $is_register) {
    if ($is_register == '1') {
        $error = __('Beim Bezahlvorgang ist ein Fehler aufgetreten, Ihr Benutzer-Login wurde aber angelegt. Bitte <a href="/anmelden/">melden Sie sich an</a> und probieren Sie die Zahlung erneut durchzuführen.', 'velvet');
    }
    return $error;
}
add_filter('pms_payment_error_message', 'velvet_change_payment_error_message', 20, 2);

/**
 * Quform: time range
 */
add_filter('quform_element_valid_1_9', function ($valid, $value, Quform_Element_Time $element) {
    if ( ! in_array($value, velvet_get_available_times())) {
        $element->setError(__('Diese Uhrzeit liegt ausserhalb unserer Öffnungszeiten.','velvet'));
        $valid = false;
    }
    return $valid;
}, 10, 3);

function velvet_get_available_times() {
    return array(
        '00:15', '00:30', '00:45', 
        '01:00', '01:15', '01:30', '01:45',
        '02:00', '11:15', '11:30', '11:45',
        '12:00', '12:15', '12:30', '12:45',
        '13:00', '13:15', '13:30', '13:45',
        '14:00', '14:15', '14:30', '14:45',
        '15:00', '15:15', '15:30', '15:45',
        '16:00', '16:15', '16:30', '16:45',
        '17:00', '17:15', '17:30', '17:45',
        '18:00', '18:15', '18:30', '18:45',
        '19:00', '19:15', '19:30', '19:45',
        '20:00', '20:15', '20:30', '20:45',
        '21:00', '21:15', '21:30', '21:45',
        '22:00', '22:15', '22:30', '22:45',
        '23:00', '23:15', '23:30', '23:45',
        '00:00'
    );
}
add_action('wp_footer', function() {
    printf('<script>var timePickerTimes = %s;</script>', wp_json_encode(velvet_get_available_times()));
});

/**
 * WP Twilio Core
 */
// general SMS sending function
function velvet_send_sms($number_to, $message) {
    $send = false;

    if ($message!='') : 
        $args = array( 
            'number_to' => $number_to,
            'message' => $message,
        ); 
        $send = twl_send_sms( $args );
    endif;

    return $send;
}
// hook into quform sending
add_filter('quform_confirmation_message_1_1', function ($message, Quform_Confirmation $confirmation, Quform_Form $form) {

    $sms_sendto = get_field('sms_sendto', 'option');
    $sms_text   = get_field('sms_text', 'option');

    velvet_send_sms($sms_sendto, $sms_text);
    return $message;
}, 10, 3);

/**
 * User: get current IP
 */
function velvet_get_the_user_ip() {
    if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
        //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
        //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return apply_filters( 'wpb_get_ip', $ip );
}
add_shortcode('show_user_ip', 'velvet_get_the_user_ip');

/**
 * Debug helper
 */
function velvet_debug( $input ) {
    echo '<pre>';
    print_r($input);
    echo '</pre>';
}